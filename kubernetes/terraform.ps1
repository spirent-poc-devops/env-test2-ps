#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 2)]
    [hashtable] $Resources
)

############################## Keypair #####################################
$keypairTerraformPath = "$PSScriptRoot/../temp/keypair_k8s_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $keypairTerraformPath)) {
    $null = New-Item $keypairTerraformPath -ItemType "directory"
}

# Select cloud terraform templates/terraform_scripts
$env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_stage = Get-EnvMapValue -Map $config -Key "aws.stage"
$env:TF_VAR_aws_namespace = Get-EnvMapValue -Map $config -Key "aws.namespace"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"

Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/provider.tf" -Destination "$keypairTerraformPath/provider.tf"
Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/keypair.tf" -Destination "$keypairTerraformPath/keypair.tf"

# Load terraform state from config folder
Sync-State -Component "keypair_k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
###################################################################

################################## Kubernetes Virtual Machine #################################
$k8sTerraformPath = "$PSScriptRoot/../temp/k8s_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $k8sTerraformPath)) {
    $null = New-Item $k8sTerraformPath -ItemType "directory"
}

# Select cloud terraform templates
switch (Get-EnvMapValue -Map $config -Key "k8s.cloud") {
    "aws" {
        $env:TF_VAR_aws_cred_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
        $env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
        $env:TF_VAR_aws_role_name = Get-EnvMapValue -Map $config -Key "aws.terraform_role_name"
        $env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
        $env:TF_VAR_k8s_instance_ami = Get-EnvMapValue -Map $config -Key "k8s.instance_ami"
        $env:TF_VAR_k8s_instance_type = Get-EnvMapValue -Map $config -Key "k8s.instance_type"
        $env:TF_VAR_k8s_keypair_name = Get-EnvMapValue -Map $resources -Key "k8s.keypair_name"
        $env:TF_VAR_k8s_subnet_id = Get-EnvMapValue -Map $config -Key "k8s.subnet_id"
        $env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
        $env:TF_VAR_aws_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"
        $env:TF_VAR_mgmt_subnet_cidr = Get-EnvMapValue -Map $config -Key "mgmtstation.subnet_cidr"
        $env:TF_VAR_mgmt_public_ip = Get-EnvMapValue -Map $resources -Key "mgmtstation.public_ip"
        $env:TF_VAR_k8s_volume_size = Get-EnvMapValue -Map $config -Key "k8s.volume_size"
        $env:TF_VAR_k8s_associate_public_ip_address = Get-EnvMapValue -Map $config -Key "k8s.associate_public_ip_address"
        $env:TF_VAR_public_hosted_zone_id = Get-EnvMapValue -Map $config -Key "environment.route53_zone_id"
        $env:TF_VAR_dns_public_name = (Get-EnvMapValue -Map $config -Key "environment.prefix").ToString() +'.'+ (Get-EnvMapValue -Map $config -Key "environment.public_domain").ToString()
        $env:TF_VAR_k8s_ebs_snapshot_id = Get-EnvMapValue -Map $config -Key "k8s.ebs_snapshot_id"

        Write-Host "$env:TF_VAR_public_hosted_zone_id $env:TF_VAR_dns_public_name" -ForegroundColor Cyan


        Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/provider.tf" -Destination "$k8sTerraformPath/provider.tf"
        if ($(Get-EnvMapValue -Map $config -Key "k8s.create_route53_ingress") -eq $true) {
            Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/k8s_vm.tf" -Destination "$k8sTerraformPath/k8s_vm.tf"
        } else {
            Copy-Item -Path "$PSScriptRoot/templates/terraform_scripts/k8s_vm_without_route53.tf" -Destination "$k8sTerraformPath/k8s_vm.tf"
        }
    }
    DEFAULT {
        Write-EnvError -Component "kubernetes" "Cloud $(Get-EnvMapValue -Map $config -Key "k8s.cloud") doesn't supported."
    }
}
# Load terraform state from config folder
Sync-State -Component "k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
###################################################################