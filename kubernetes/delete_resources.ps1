#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

################################## Kubernetes Virtual Machine ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws k8s resources
Set-Location $k8sTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$k8sTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$k8sTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't delete cloud resources. Watch logs above or check '$k8sTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "k8s") {
    Remove-EnvMapValue -Map $resources -Key "k8s.private_ip"
    Remove-EnvMapValue -Map $resources -Key "k8s.public_ip"
    Remove-EnvMapValue -Map $resources -Key "k8s.id"
    Remove-EnvMapValue -Map $resources -Key "k8s.sg_id"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
###################################################################

################################## Keypair ##################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

Set-Location $keypairTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$keypairTerraformPath' folder content."
}

terraform destroy -auto-approve
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't delete cloud resources. Watch logs above or check '$keypairTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "keypair_k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

# Delete results and save resource file to disk
if (Test-EnvMapValue -Map $resources -Key "k8s") {
    Remove-EnvMapValue -Map $resources -Key "k8s.keypair_name"
    Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
}
###################################################################
