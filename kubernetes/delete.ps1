#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

#  Delete service
. "$PSScriptRoot/delete_service.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath

# Delete resources
. "$PSScriptRoot/delete_resources.ps1" -ConfigPath $ConfigPath -ResourcePath $ResourcePath
