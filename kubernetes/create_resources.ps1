#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

############################## Keypair #####################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Initialize terraform
Set-Location $keypairTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check './temp/$keypairTerraformPath' folder content"
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check './temp/$keypairTerraformPath' folder content"
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't create cloud resources. Watch logs above or check './temp/$keypairTerraformPath' folder content"
}

# Get keypair name
$keypair=$(terraform output keypair_name).Trim('"')
Set-EnvMapValue -Map $resources -Key "k8s.keypair_name" -Value $keypair

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "keypair_k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################

################################## Kubernetes Virtual Machine #################################
# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Initialize terraform
Set-Location $k8sTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$k8sTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$k8sTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't create cloud resources. Watch logs above or check '$k8sTerraformPath' folder content."
}

# Save results to resource file
$k8s_id=$(terraform output "k8s_id").Trim('"')
Set-EnvMapValue -Map $resources -Key "k8s.id" -Value $k8s_id
$k8s_private_ip=$(terraform output "k8s_private_ip").Trim('"') 
Set-EnvMapValue -Map $resources -Key "k8s.private_ip" -Value $k8s_private_ip
$k8s_public_ip=$(terraform output "k8s_public_ip").Trim('"')
Set-EnvMapValue -Map $resources -Key "k8s.public_ip" -Value $k8s_public_ip
$k8s_sg_id=$(terraform output "k8s_sg_id").Trim('"')
Set-EnvMapValue -Map $resources -Key "k8s.sg_id" -Value $k8s_sg_id

# Open access by public ip
$env:AWS_DEFAULT_REGION = Get-EnvMapValue -Map $config -Key "aws.region"
aws ec2 authorize-security-group-ingress --group-id $k8s_sg_id --protocol tcp --port 0-65535 --cidr "$k8s_public_ip/32"

Set-Location "$PSScriptRoot/.."

# Save terraform state to config folder
Save-State -Component "k8s" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
###################################################################
