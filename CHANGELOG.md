# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> Spirent VisionWorks Test Environment ChangeLog

### 2.0.0 (2021-11-24)

Second major update of the test environment where all infrastructure services are moved out into a separate Helm chart

#### Features
* Kubernetes single-instance cluster

#### Breaking Changes
All infrastructure services were moved to a package-infraservices-helm component where they are installed in dockerized form by a helm chart

### 1.0.0 (2021-08-30)

Initial release of the environment template

#### Features
* Create, update and delete scripts
* Kubernetes single-instance cluster
* Timescale single-instance configuration
* Kafka message broker running in Kubernetes
* Redis distributed cache running in Kubernetes

#### Breaking Changes
No breaking changes since this is the first version

#### Bug Fixes
No fixes in this version

